﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

/// <summary>
/// Handles network related things in a PUN loaded scene.
/// </summary>
public class NetworkManager : MonoBehaviour
{
	void Start()
	{
		SpawnPlayer();
	}

	public void SpawnPlayer()
	{
		PhotonNetwork.Instantiate("Prefabs/Player", Vector3.zero, Quaternion.identity);
	}
}
