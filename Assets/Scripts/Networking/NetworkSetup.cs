﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

/// <summary>
/// Responsible for quickly setting up a connection to the PUN server & rooms.
/// </summary>
public class NetworkSetup : MonoBehaviourPunCallbacks
{
	[SerializeField] int roomSize;
	[SerializeField] int networkedSceneIndex;
	private void Start()
	{
		PhotonNetwork.ConnectUsingSettings();
	}

	public override void OnConnectedToMaster()
	{
		PhotonNetwork.AutomaticallySyncScene = true;
		Debug.Log("Connected.");
	}

	public void JoinRandomRoom()
	{
		PhotonNetwork.JoinRandomRoom();
	}
	public override void OnJoinRandomFailed(short returnCode, string message)
	{
		Debug.LogWarning("Failed to join room, trying again.");
		CreateRoom();
	}

	public void CreateRoom()
	{
		RoomOptions roomOptions = new RoomOptions()
		{
			IsVisible = true,
			IsOpen = true,
			MaxPlayers = (byte)roomSize,
			
		};

		PhotonNetwork.CreateRoom("AoATestThing", roomOptions);
	}
	public override void OnCreateRoomFailed(short returnCode, string message)
	{
		Debug.Log("Failed to create room, trying again.");
		CreateRoom();
	}

	public override void OnJoinedRoom()
	{
		if (PhotonNetwork.IsMasterClient)
		{
			PhotonNetwork.LoadLevel(networkedSceneIndex);
		}
	}
}
