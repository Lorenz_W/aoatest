﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResizingDropdown : MonoBehaviour
{
	/// <summary>
	/// Changes the height & width of the "Dropdown List" from this objects dropdown component.
	/// New height/width accomodates all options without anything being cut off or requiring scrolling to see.
	/// Call through an EventTrigger component on the dropdown or add to some custom event.
	/// </summary>
	public void Resize()
	{
		Dropdown dropdown = this.GetComponent<Dropdown>();
		RectTransform toResize = (RectTransform)dropdown.transform.Find("Dropdown List");
		Debug.Log("To resize: " + toResize.gameObject.name);

		float previousPrefWidth = 0f;
		Text[] optionTexts = toResize.GetComponentsInChildren<Text>();
		foreach (Text option in optionTexts)
		{
			// Either force change here or be aware that resizing doesn't work with the default before changing
			// it in "Template".
			option.horizontalOverflow = HorizontalWrapMode.Overflow;

			if (option.preferredWidth > previousPrefWidth)
			{
				previousPrefWidth = option.preferredWidth;

				//Debug.Log(" " + toResize.offsetMax.x + " | " + toResize.rect.x + " | " + option.preferredWidth
				//+ " | " + option.rectTransform.rect.x);

				float currentWidth = Mathf.Abs(option.rectTransform.rect.x) + Mathf.Abs(toResize.rect.x);

				//Expands toResize along its x-axis.
				toResize.offsetMax += new Vector2(option.preferredWidth - currentWidth, 0);
			}
		}

		// Going to parent.parent to get the Content object because I'm not aware of a cleaner way to get this
		// value. Tho there are lots of other, equally dirty ways.
		float contentSizeY = optionTexts[0].transform.parent.parent.GetComponent<RectTransform>().rect.height;

		//Debug.Log(" " + toResize.offsetMin.y + " | " + toResize.rect.y + " | " + contentSizeY);

		// Expands toResize along its y-axis.
		toResize.offsetMin -= new Vector2(0, contentSizeY + toResize.offsetMin.y);
	}
}
