﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Player : MonoBehaviour
{
	[SerializeField] Camera playerCam;
	[SerializeField] string inputAxis_Vertical;
	[SerializeField] string inputAxis_Horizontal;
	[SerializeField] string inputAxis_rotateHorizontal;
	[SerializeField] string inputAxis_camVertical;

	[SerializeField] float moveSpeed = 10f;
	[SerializeField] float rotateSpeed = 5f;
	[SerializeField] float camVerticalSpeed = 2f;

	Rigidbody rigidB;
	PhotonView pv;

	void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
		rigidB = this.gameObject.GetComponent<Rigidbody>();
		pv = this.gameObject.GetComponent<PhotonView>();

		//Disabling things that would be problematic to hav on active for players that don't own this player object.
		if (!pv.IsMine)
		{
			rigidB.isKinematic = true;
			playerCam.gameObject.SetActive(false);
		}
	}

	void Update()
	{
		if (pv.IsMine && Cursor.lockState == CursorLockMode.Locked)
		{
			Vector3 desiredMove = new Vector3(Input.GetAxisRaw(inputAxis_Horizontal), 0, Input.GetAxisRaw(inputAxis_Vertical));
			Walk(desiredMove);

			Vector3 desiredRotation = new Vector3(0.0f, Input.GetAxisRaw(inputAxis_rotateHorizontal) * rotateSpeed, 0.0f);
			Rotate(desiredRotation);

			Vector3 desiredCamMove = new Vector3(Input.GetAxis(inputAxis_camVertical) * -1 * camVerticalSpeed, 0.0f, 0.0f);
			CamMovement(desiredCamMove);
		}
	}

	void Walk(Vector3 move)
	{
		if (move.y == 0)
		{
			move.y = rigidB.velocity.y;
		}

		Vector3 tmp = Vector3.zero;
		tmp += this.gameObject.transform.forward * move.z * moveSpeed;
		tmp += this.gameObject.transform.right * move.x * moveSpeed;
		tmp += this.gameObject.transform.up * move.y;

		rigidB.velocity = tmp;
	}

	void Rotate(Vector3 eulerRotation)
	{
		this.gameObject.transform.Rotate(eulerRotation);
	}

	void CamMovement(Vector3 move)
	{
		playerCam.gameObject.transform.Rotate(move);
	}
}
